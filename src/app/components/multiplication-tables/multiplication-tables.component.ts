import {Component, Input, OnChanges} from '@angular/core';

@Component({
  selector: 'app-multiplication-tables',
  templateUrl: './multiplication-tables.component.html',
  styleUrls: ['./multiplication-tables.component.scss']
})
export class MultiplicationTablesComponent implements OnChanges {

  @Input() multiplicationsFormNumberValue!: number;

  // tslint:disable-next-line:no-any
  tables: any = [];

  constructor() {}

  getMultiplicationTables(multiplicationsFormNumberValue: number): [] {

    for (let i = 1; i <= multiplicationsFormNumberValue; i++){
      // tslint:disable-next-line:no-any
      const table: any = {
        name: '',
        calc: []
      };

      table.name = 'Table of ' + i;
      for (let j = 1; j <= 10; j++) {
        table.calc.push(i + ' x ' + j + ' = ' + i * j);
      }

      this.tables.push(table);
    }

    return this.tables;
  }

  ngOnChanges(): void {
    this.tables = [];
    this.getMultiplicationTables(this.multiplicationsFormNumberValue);
  }

}


