import {Component} from '@angular/core';
import {FormGroup, FormControl, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  multiplicationForm: FormGroup;
  multiplicationFormNumberIsEmpty = true;
  multiplicationFormNumberChanged = false;
  multiplicationFormNumberIsValid = false;
  multiplicationFormNumberValue = 1;

  constructor(private formBuilder: FormBuilder) {
    this.multiplicationForm = this.formBuilder.group({
      number: new FormControl()
    });
    this.verifMultiplicationForm(this.multiplicationForm, 1, 100);
  }

  verifMultiplicationForm(form: FormGroup, minValue: number, maxValue: number): void{
    form.get('number')?.valueChanges.subscribe(() => {
      this.multiplicationFormNumberIsEmpty = false;
      this.multiplicationFormNumberChanged = true;
      this.multiplicationFormNumberIsValid = false;

      if (!form.get('number')?.value && form.get('number')?.value !== 0){
        this.multiplicationFormNumberIsEmpty = true;
      }else if (form.get('number')?.value >= minValue && form.get('number')?.value <= maxValue){
        this.multiplicationFormNumberIsEmpty = false;
        this.multiplicationFormNumberIsValid = true;
        this.multiplicationFormNumberValue = form.get('number')?.value;
      }
    });
  }

}
