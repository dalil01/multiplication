import {Component, Input, OnChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-multiplication-table',
  templateUrl: './multiplication-table.component.html',
  styleUrls: ['./multiplication-table.component.scss']
})
export class MultiplicationTableComponent implements OnChanges {

  @Input() multiplicationFormNumberValue!: number;
  @Input() multiplicationFormNumberIsValid!: boolean;

  multiplicationsForm: FormGroup;
  multiplicationsFormNumberIsEmpty = true;
  multiplicationsFormNumberChanged = false;
  multiplicationsFormNumberIsValid = false;
  multiplicationsFormNumberValue = 10;

  tables: string[] = [];

  constructor(private formBuilder: FormBuilder) {
    this.multiplicationsForm = this.formBuilder.group({
      number: new FormControl()
    });
    this.verifMultiplicationsForm(this.multiplicationsForm, 10, 100);
  }

  getMultiplicationTable(multiplicationFormNumberValue: number): string[]{
    this.tables = [];
    for (let i = 1; i <= 10; i++){
      this.tables.push(multiplicationFormNumberValue + ' x ' + i + ' = ' + multiplicationFormNumberValue * i);
    }
    return this.tables;
  }

  ngOnChanges(): void {
    this.getMultiplicationTable(this.multiplicationFormNumberValue);
  }

  verifMultiplicationsForm(form: FormGroup, minValue: number, maxValue: number): void{
    form.get('number')?.valueChanges.subscribe(() => {
      this.multiplicationsFormNumberIsEmpty = false;
      this.multiplicationsFormNumberChanged = true;
      this.multiplicationsFormNumberIsValid = false;

      if (!form.get('number')?.value && form.get('number')?.value !== 0){
        this.multiplicationsFormNumberIsEmpty = true;
      }else if (form.get('number')?.value >= minValue && form.get('number')?.value <= maxValue){
        this.multiplicationsFormNumberIsEmpty = false;
        this.multiplicationsFormNumberIsValid = true;
        this.multiplicationsFormNumberValue = form.get('number')?.value;
      }
    });
  }

}
