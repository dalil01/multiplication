import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './components/app.component';
import { MultiplicationTableComponent } from './components/multiplication-table/multiplication-table.component';
import { MultiplicationTablesComponent } from './components/multiplication-tables/multiplication-tables.component';

@NgModule({
  declarations: [
    AppComponent,
    MultiplicationTableComponent,
    MultiplicationTablesComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
