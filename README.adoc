# Multiplication

Réalisé par Chablis Dalil entre le 9 et le 12 novembre 2020.

*Exercice de programmation* destiné à permettre à son développeur de mettre en application les premiers concepts liés au framework *Angular*.

## Objectif

Conception d'une application web mono-page affichant une table de multiplication (1 à 10), selon
une valeur soumise par l'utilisateur.

*Partie 1 :*
La page principale fera usage du composant
*MultiplicationTable* dont le rôle est de présenter une table de multiplication.
Ce composant devra être paramétré : il exploitera une valeur qui lui est transmise par son parent :
une valeur entière qui correspond à la table à afficher.
L’application devra permettre à l’utilisateur de saisir une valeur pour l’affichage de cette table de
multiplication.

*Partie 2 :*
Dans une seconde partie, on présentera, en dessous du résultat précédent, les tables de
multiplication. Le composant *MultiplicationTables* sera paramétré (valeur transmise par son parent) et
l’utilisateur aura la possibilité de saisir le nombre de tables à afficher (10 par défaut).

## Frameworks
    * Bulma (https://bulma.io/)
    * Angular (https://angular.io/)

## Exigence
    * Node Js (npm)
    * CLI Angular
    * (Optionnel) Git (Pour cloner)

## Parlons code...

Diagramme de classes UML des composants .ts de l'application (ASCII Art).

```
,----------------------------------------.
|              AppComponent              |
|----------------------------------------|
|multiplicationForm: FormGroup           |
|multiplicationFormNumberIsEmpty: boolean|
|multiplicationFormNumberChanged: boolean|
|multiplicationFormNumberIsValid: boolean|
|multiplicationFormNumberValue: number   |
|----------------------------------------|
|verifMultiplicationFrom(): void         |
`----------------------------------------'
                     |
                     |  multiplicationFormNumberIsValid
                     |  multiplicationFormNumberValue
                     |
                     v
,-----------------------------------------.
|        MultiplicationTableComponent     |
|-----------------------------------------|
|multiplicationsForm: FormGroup           |
|multiplicationsFormNumberIsEmpty: boolean|
|multiplicationsFormNumberChanged: boolean|
|multiplicationsFormNumberIsValid: boolean|
|multiplicationsFormNumberValue: number   |
|                                         |
|tables: string[]                         |
|-----------------------------------------|
|verifMultiplicationsFrom(): void         |
|getMultiplicationTable(): string[]       |
|ngOnChanges(): void                      |
`-----------------------------------------'
                     |
                     |  multiplicationsFormNumberValue
                     |
                     v
      ,-----------------------------.
      |MultiplicationTablesComponent|
      |-----------------------------|
      |tables: any                  |
      |-----------------------------|
      |getMultiplicationTables(): []|
      |ngOnChanges(): void          |
      `-----------------------------'
```

*Structure du dossier src/app :*

    .
    └── src
        └──app
            └── components
            │   │
            │   ├── multiplication-table
            │   │   ├── multiplication-table.html
            │   │   ├── multiplication-table.scss
            │   │   ├── multiplication-table.spec.ts
            │   │   └── multiplication-table.ts
            │   │
            │   ├── multiplication-tables
            │   │   ├── multiplication-tables.html
            │   │   ├── multiplication-tables.scss
            │   │   ├── multiplication-tables.spec.ts
            │   │   └── multiplication-tables.ts
            │   │
            │   ├── app.component.html
            │   ├── app.component.scss
            │   ├── app.component.spec.ts
            │   └── app.component.ts
            │
            ├── app.module.ts
            └── package.json

### Partie 1

*app.component.ts*

```
    [...]

    multiplicationForm: FormGroup;
    multiplicationFormNumberIsEmpty = true;
    multiplicationFormNumberChanged = false;
    multiplicationFormNumberIsValid = false;
    multiplicationFormNumberValue = 1;
```

Déclaration des attributs liés au formulaire permettant l'affichage de la table de multiplication.

```
constructor(private formBuilder: FormBuilder) {
this.multiplicationForm = this.formBuilder.group({
  number: new FormControl()
});
this.verifMultiplicationForm(this.multiplicationForm, 1, 100);
}
```

Utilisation du *ReativeFormsModule* (Import éffectuée dans *app.module.ts*).

Appel à la méthode *verifMultiplicationForm()*.

```
  verifMultiplicationForm(form: FormGroup, minValue: number, maxValue: number): void{
    form.get('number')?.valueChanges.subscribe(() => {
      this.multiplicationFormNumberIsEmpty = false;
      this.multiplicationFormNumberChanged = true;
      this.multiplicationFormNumberIsValid = false;

      if (!form.get('number')?.value && form.get('number')?.value !== 0){
        this.multiplicationFormNumberIsEmpty = true;
      }else if (form.get('number')?.value >= minValue && form.get('number')?.value <= maxValue){
        this.multiplicationFormNumberIsEmpty = false;
        this.multiplicationFormNumberIsValid = true;
        this.multiplicationFormNumberValue = form.get('number')?.value;
      }
    });
  }
```

*verifMultiplicationForm()* a pour objectif de souscrire à l'événement *valueChanges* de l'input nommée "number", et de vérifier si le contenu saisi par l'utilisateur correspond au contenu attendu :

* Le nombre saisi doit être positif et inférieur à 101.

Ces contraintes ont été ajoutées afin de tester le dynamisme du formulaire.

*app.component.html*

```
[...]

<form [formGroup]="multiplicationForm">
<div class="field has-text-centered">
  <div class="field-body">
    <div class="field">
      <p class="control is-expanded">
        <input [ngClass]="{'input': true, 'is-light': multiplicationFormNumberIsEmpty, 'is-success': multiplicationFormNumberChanged && multiplicationFormNumberIsValid, 'is-danger': !multiplicationFormNumberIsEmpty && multiplicationFormNumberChanged && !multiplicationFormNumberIsValid}" style="width: 20%;" type="number" value="33" formControlName="number" min="1" max="10" autofocus>
      </p>
    </div>
  </div>
</div>
</form>

[...]
```

La directive *[ngClass]* permet ici le traitement dynamique du style de l'input selon l'état de l'attribut défini dans *app.component.ts* (true / false).

```

  <app-multiplication-table [multiplicationFormNumberIsValid]="multiplicationFormNumberIsValid"  [multiplicationFormNumberValue]="multiplicationFormNumberValue"></app-multiplication-table>
</main>

[...]

```

Inclusion du composant enfant *multiplication-table* + transmission des attributs *multiplicationFormNumberIsValid* et *multiplicationFormNumberValue*.

*multiplication-table.component.ts*

```
[...]

  @Input() multiplicationFormNumberValue!: number;
  @Input() multiplicationFormNumberIsValid!: boolean;

[...]
```

Récupération des attributs envoyés par le parent.

```
getMultiplicationTable(multiplicationFormNumberValue: number): string[]{
    this.tables = [];
    for (let i = 1; i <= 10; i++){
      this.tables.push(multiplicationFormNumberValue + ' x ' + i + ' = ' + multiplicationFormNumberValue * i);
    }
    return this.tables;
  }
```

La méthode *getMultiplicationTable()* est chargée d'insérer dans l'attribut *tables* une chaîne représentant le calcul (Ex: "2 x 2 = 4"). Elle reçoit en paramètre le nombre reçu du parent.

```
  ngOnChanges(): void {
    this.getMultiplicationTable(this.multiplicationFormNumberValue);
  }

  [...]
```

Utilisation du lifecycle-hook *ngOnChanges* d'Angular dans le but d'appeler la méthode *getMultiplicationTable()* après chaque changement.

Une méthode *verifMultiplicationsForm()* existe également dans cette classe et son rôle est de souscrire à l'événement *valueChanges* de l'input nommée "number" et de vérifier si le contenu saisi par l'utilisateur correspond à ce qui est attendu (affichage de la table de multiplication).

*multiplication-table.component.html*

```
<div *ngIf="multiplicationFormNumberIsValid; else elseBlock">
  <h3 class="has-text-centered is-size-4 has-text-weight-bold">Table of {{ multiplicationFormNumberValue }}</h3>
  <ul class="has-text-centered">
    <li *ngFor="let multi of tables">{{ multi }}</li>
  </ul>

    <ng-template #elseBlock>
      <div class="pb-5">
        <p class="bd-notification is-primary">
          <strong class="is-size-5">A multiplication table</strong> displays in the rows and columns the result of the multiplication of small natural whole numbers. The term used from the Middle Ages to the 16th century was "booklet".
          <br><br>
          The decimal position numbering system makes it possible to multiply any two numbers using only the knowledge of the products of the numbers 0 to 9 between them. It is in elementary school that the learning of the tables which summarize all these products is carried out. The tradition has long required the knowledge of multiplication tables with up to 12 or 13 instead of 9.
          <br><br>
          Source : <a href="https://fr.wikipedia.org/wiki/Table_de_multiplication" target="_blank">https://fr.wikipedia.org/wiki/Table_de_multiplication</a>.
        </p>
      </div>
    </ng-template>
</div>
```

Utilisation de la directive *ngIf* pour afficher soit un texte de description des tables de multiplication, soit les tables de multiplication, en bouclant sur l'attribut *tables* avec *ngFor*.

```

<section class="pt-3 is-light" *ngIf="multiplicationFormNumberIsValid">
  <hr>
    <h3 class="pt-5 has-text-centered subtitle">Choose between (10 and 100)</h3>
    <form [formGroup]="multiplicationsForm">
      <div class="field has-text-centered">
        <div class="field-body">
          <div class="field">
            <p class="control is-expanded">
              <input [ngClass]="{'input': true, 'is-light': multiplicationsFormNumberIsEmpty, 'is-success': multiplicationsFormNumberChanged && multiplicationsFormNumberIsValid, 'is-danger': !multiplicationsFormNumberIsEmpty && multiplicationsFormNumberChanged && !multiplicationsFormNumberIsValid }" style="width: 15%;" type="number" formControlName="number" min="10" max="100" autofocus>
            </p>
          </div>
        </div>
      </div>
    </form>
  <app-multiplication-tables [multiplicationsFormNumberValue]="multiplicationsFormNumberValue"></app-multiplication-tables>
</section>
```

### Partie 2

Formulaire permettant de transmettre le nombre souhaité de table à *multiplication-tables* .

*multiplication-tables.component.ts*

```
  @Input() multiplicationsFormNumberValue!: number;
```

Réception du nombre envoyé par le parent (*multiplication-table*).

```
tables: any = [];

  constructor() {}

  getMultiplicationTables(multiplicationsFormNumberValue: number): [] {

    for (let i = 1; i <= multiplicationsFormNumberValue; i++){
      // tslint:disable-next-line:no-any
      const table: any = {
        name: '',
        calc: []
      };

      table.name = 'Table of ' + i;
      for (let j = 1; j <= 10; j++) {
        table.calc.push(i + ' x ' + j + ' = ' + i * j);
      }

      this.tables.push(table);
    }

    return this.tables;
  }

[...]
```

De manière similaire au fonctionnement de *getMultiplicationTable*, *getMultiplicationTables* se charge d'insérer l'objet *table* dans une table (*tables*).
Cet objet a été créé à des fins d'affichage.

*multiplication-tables.component.html*

```
  [...]
  <article class="mt-5 pt-4 pb-5 is-multiline columns">
    <div class="column is-2 mb-5" *ngFor="let multi of tables">
      <p class="has-text-weight-bold">{{ multi.name }}</p>
      <p *ngFor="let m of multi.calc">{{ m }}</p>
    </div>
  </article>
```

## Conclusion

Cet exercice m'a permis de mieux comprendre le fonctionnement du framework *Angular*, et de mettre en application les premiers concepts qui s'y rapportent.

Le typage strict avec le *Typecript* m'a notamment créé plusieurs failles de compilation, mais une fois compilée, la page se recharge automatiquement, ce qui est très pratique.

Une des difficultés que j'ai rencontrées a été l'envoi des données du composant parent à l'enfant. En effet, lorsque ces données sont modifiées au niveau du parent, elles ne sont pas modifiées au niveau de l'enfant.
Mais cela a rapidement été résolu grâce à la documentation *Angular*. Il a fallu implémenté la méthode onChanges afin de récupérer les changements opérés (https://angular.io/guide/lifecycle-hooks#onchanges).

Une autre difficulté était dans l'affichage des tables de multiplication (Partie 2), je voulais utiliser les classes du framework *Bulma*, afin de les afficher sur plusieurs colonnes et non sur une seule.
J'ai donc une première table qui contient des objets, dont la clé est le nom de la table (Ex : "Table of 6") et la valeur est une table contenant la table de multiplication.

## Installation
* git clone https://gitlab.com/dalil01/multiplication
* npm install

## Usage
Démarrage et ouverture du serveur dans le navigateur :

* ng s -o


